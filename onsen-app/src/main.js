import Vue from 'vue'
import App from './App.vue'

//Onsen
import 'onsenui/css/onsenui.css';
import 'onsenui/css/onsen-css-components.css';
import VOnsNavigator from 'vue-onsenui/esm/components/VOnsNavigator';
import VOnsPage from 'vue-onsenui/esm/components/VOnsPage'
import VOnsToolbar from 'vue-onsenui/esm/components/VOnsToolbar'
import VueOnsen from 'vue-onsenui';

Vue.config.productionTip = false

//Onsen
Vue.use(VueOnsen)
Vue.component(VOnsNavigator.name, VOnsNavigator)
Vue.component(VOnsPage.name, VOnsPage)
Vue.component(VOnsToolbar.name, VOnsToolbar);

new Vue({
    render: h => h(App),
}).$mount('#app')